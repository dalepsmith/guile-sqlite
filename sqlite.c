/*	Copyright (C) 2004,2010 Dale P. Smith
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307 USA */

#include <string.h>
#include <sqlite.h>
#include <libguile.h>

static scm_t_bits scm_tc16_sqlite_db =0;

SCM_DEFINE(gs_sqlite_open, "sqlite-open", 1, 1, 0, (SCM filename, SCM ro),
	    "")
#define FUNC_NAME s_gs_sqlite_open
{
    sqlite *db = NULL;
    char *message = NULL;
    int mode;

    SCM_VALIDATE_STRING(SCM_ARG1, filename);
    SCM_VALIDATE_BOOL(SCM_ARG2, ro);

    mode = scm_is_false(ro) ? 0666 : 0444;	// verify this

    db = sqlite_open(scm_to_locale_string(filename), mode, &message);
    if (!db) {
	char message_buffer[128];

	strcpy(message_buffer, message);
	free(message);
	scm_error(SCM_BOOL_T, FUNC_NAME,	/* Verify this! */
		  message_buffer, SCM_BOOL_F, SCM_BOOL_F);
	/* never returns */
    }

    SCM_RETURN_NEWSMOB(scm_tc16_sqlite_db, db);
}

#undef FUNC_NAME

SCM_DEFINE(gs_sqlite_close, "sqlite-close", 1, 0, 0, (SCM db), "")
#define FUNC_NAME s_gs_sqlite_close
{
    SCM_VALIDATE_SMOB(SCM_ARG1, db, sqlite_db);

    sqlite_close((sqlite *) SCM_SMOB_DATA(db));
    return SCM_UNSPECIFIED;
}

#undef FUNC_NAME

struct helper {
    SCM closure;
    SCM tag;
    SCM throw_args;
};

static SCM
gs_exec_helper_handler(void *data, SCM tag, SCM throw_args)
{
    struct helper *h = data;

    h->tag = tag;
    h->throw_args = throw_args;

    return SCM_BOOL_F;
}


struct sqlite_callback_data {
    SCM closure;
    SCM argc;
    SCM vec;
    SCM cols;
};
  

static SCM
gs_exec_helper_body(void *data)
{
    struct sqlite_callback_data *scd = data;

    scm_apply_3(scd->closure, scd->argc, scd->vec, scd->cols, SCM_EOL);
    return SCM_BOOL_T;
}


static int
gs_exec_helper(void *help_data, int argc, char **argv, char **colname)
{
    SCM ret;
    int i;
    struct sqlite_callback_data scd;
     
    if (!argv) {
	scd.vec = SCM_BOOL_F;
    } else {
	scd.vec = scm_c_make_vector(argc, SCM_UNSPECIFIED);
	for (i = 0; i < argc; ++i) {
	    SCM_SIMPLE_VECTOR_SET(scd.vec, i,
				  argv[i]
				  ? scm_from_locale_stringn(argv[i],
							    strlen(argv[i]))
				  : SCM_BOOL_F);
	}
    }
     
    // Hmm.  This is wasteful to make copies of the column names for
    // ever row processed.  The C callback just uses (if it want's
    // to) the strings. They are only allocated once by sqlite_exec.
    scd.cols = scm_c_make_vector(argc, SCM_UNSPECIFIED);
    for (i = 0; i < argc; ++i) {
	SCM_SIMPLE_VECTOR_SET(scd.cols, i,
			      scm_from_locale_stringn(colname[i],
						      strlen(colname[i])));
    }
     
    scd.closure = ((struct helper *) help_data)->closure;
    scd.argc = scm_from_int(argc);
     
    ret = scm_internal_catch(SCM_BOOL_T,
			     gs_exec_helper_body, &scd,
			     gs_exec_helper_handler, help_data);
     
    return scm_is_false(ret);	/* non 0 for abort */
}


SCM_DEFINE(gs_sqlite_exec, "sqlite-exec", 3, 0, 0,
	   (SCM db, SCM query, SCM callback), "")
#define FUNC_NAME s_gs_sqlite_exec
{
    int ret = 0;
    char *message = NULL;
    struct helper helper_data;
    SCM arity;

    SCM_VALIDATE_SMOB(SCM_ARG1, db, sqlite_db);
    SCM_VALIDATE_STRING(SCM_ARG2, query);

    arity = scm_procedure_minimum_arity(callback);
    SCM_ASSERT(scm_to_int(SCM_CAR(arity)) == 3, callback, SCM_ARG3, FUNC_NAME);

    helper_data.closure = callback;
    helper_data.tag = SCM_EOL;
    helper_data.throw_args = SCM_EOL;

    ret = sqlite_exec((sqlite *) SCM_SMOB_DATA(db), scm_to_locale_string(query),
		      gs_exec_helper, (void *) &helper_data, &message);

    // Do something with the error message, if any
    if (ret == SQLITE_ABORT) {
	// Re-throw the error from help_data.throw_args
	// Filled in from the catch handler
	scm_throw(helper_data.tag, helper_data.throw_args);
    }

    return scm_from_int(ret);
}

#undef FUNC_NAME


void
gs_init_sqlite()
{
    scm_tc16_sqlite_db = scm_make_smob_type("sqlite-db", 0);

#include "sqlite.x"
}
